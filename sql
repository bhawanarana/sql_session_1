//table creation table level constraint
create table s_detail
(
adm_no number(5),
constraint ad_pk primary key(adm_no),
f_name varchar(20) not null,
l_name varchar(20),
dob date default sysdate,
gender char(1) 
constraint c2 check(gender in('M','m','f','F')),
roll_no varchar(10)
);



insert into s_detail values (103,'bhawana','rana','24-apr-1975','F',1);
insert into s_detail(adm_no, f_name,gender) values(104,'hema','F');
insert into s_detail(adm_no,f_name,gender) values(105,'himani','F');
insert into s_detail values (106,'himani','bhatt','25-apr-1975','F',5);
insert into s_detail values (10,'himani','bhat','25-apr-1975','F',5);


//table creation 
create table state
(
s_code varchar(3) constraint c3 primary key,
s_name varchar(10) not null
);



insert into state values(01,'M-arashtra');
insert into state values(02,'Kerala');
insert into state values(03,'Goa');
insert into state values(04,'Delhi');
insert into state values(05,'Gujarat');
insert into state values(06,'Manipur');



create table city 
(
c_code varchar(6) constraint c4 primary key,
c_name varchar(10) not null,
s_code varchar(3) constraint c5 references state(s_code)); 
 --constarint c5 foreign key (s_code) references state(s_code)

insert into city values(02,'trivandrum',2);
insert into city values(03,'Panaji',3);
insert into city values(05,'Gdhinagar',5);
insert into city values(01,'Mumbai',1);


create table s_marks (
adm_no number (5),
marks1 number(4)
constraint c7 check(marks1 between 0 and 100),
marks2 number(4)
constraint c8 check(marks2 between 0 and 100),
marks3 number (4)
constraint c9 check(marks3 between 0 and 100),
test_no number(2),
constraint c6 foreign key(adm_no) references s_detail(adm_no),
primary key(adm_no,test_no));




insert into s_marks values(103,65,65,85,01);
insert into s_marks values(104,65,68,85,01);
insert into s_marks values(105,69,65,85,01);
insert into s_marks values(106,65,87,45,01);

insert into s_marks values(103,65,65,85,02);
insert into s_marks values(104,65,68,85,02);
insert into s_marks values(105,69,65,85,02);
insert into s_marks values(106,65,87,45,02);


insert into s_marks values(103,65,65,85,03);
insert into s_marks values(104,65,68,85,03);
insert into s_marks values(105,69,65,85,03);
insert into s_marks values(106,65,87,45,03);


create table subject(
sub_nam varchar(20) NOT NULL,
PASS_MARK NUMBER (4)
CONSTRAINT C11 check(pass_mark between 0 and 100)
);


insert into subject values('English',35,01);
insert into subject values('Hindi',30,02);
insert into subject values('Maths',45,03);
insert into subject values('Science',40,04);




create table s_connect(
adm_no number(5),
constraint c12 foreign key(adm_no) references s_detail(adm_no),
c_code varchar(5),
constraint c13 foreign key(c_code) references city(c_code),
sub_code1 number(3),
constraint c14 foreign key(sub_code1) references subject(sub_id),
sub_code2 number(3),
constraint c15 foreign key(sub_code2) references subject(sub_id),
sub_code3 number(3),
constraint c16 foreign key(sub_code3) references subject(sub_id)
);



insert into s_connect values(103,01,01,02,04);
insert into s_connect values(104,02,03,03,02);
insert into s_connect values(105,03,02,01,03);
insert into s_connect values(106,01,03,03,01);


alter table subject
add sub_id number(3);

alter table subject
add constraint c10 primary key (sub_id);







